package org.emotionalpatrick.lib.threads;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

/**
 * Manages basic threading utilities and functionality.
 * 
 * @author Ryan Mulnick
 * @since Jul 4, 2013
 */
public class ThreadManager<T> {
	private ExecutorService original, service;

	public ThreadManager(final ExecutorService service) {
		this.service = service;
		this.original = this.service;
	}

	/**
	 * Safely restarts the thread manager.
	 */
	public void restart() {
		this.service.shutdownNow();
		this.service = this.original;
	}

	/**
	 * Submit the specified implementation type.
	 * 
	 * @param implementationType
	 */
	public void submit(final T implementationType) {
		if ((implementationType instanceof Runnable)) {
			this.service.submit((Runnable) implementationType);
		} else if ((implementationType instanceof Callable)) {
			this.service.submit((Callable<?>) implementationType);
		}
	}

	/**
	 * Directly execute an implementation of Runnable.
	 * 
	 * @param implementationType
	 */
	public void execute(final Runnable runnable) {
		this.service.execute(runnable);
	}

}