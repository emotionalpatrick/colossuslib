package org.emotionalpatrick.lib.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

/**
 * Reading/writing, URL reading, etc.
 * 
 * @author Ruben Church
 * 
 */
public class IOManager {
	private File file;
	private URL url;

	private PrintWriter printWriter;
	private BufferedReader bufferedReader;

	public IOManager(final URL url) {
		if (url != null)
			this.url = url;
	}

	public IOManager(final File file) {
		if (file != null)
			this.file = file;
	}

	/**
	 * Opens the write streams.
	 */
	public void openWriteStream() {
		try {
			if (this.file != null)
				this.printWriter = new PrintWriter(new FileWriter(this.file));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Closes the write streams.
	 */
	public void closeWriteStreams() {
		try {
			if (this.printWriter != null)
				this.printWriter.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Opens the read streams.
	 */
	public void openReadStreams() {
		try {
			if (this.file != null)
				this.bufferedReader = new BufferedReader(new FileReader(
						this.file));
			else
				this.bufferedReader = new BufferedReader(new InputStreamReader(
						this.url.openStream()));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Closes the read streams.
	 */
	public void closeReadStreams() {
		try {
			if (this.bufferedReader != null)
				this.bufferedReader.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes objects to the specified file.
	 * 
	 * @param object
	 */
	public void writeLine(final Object object) {
		try {
			if (this.printWriter != null)
				this.printWriter.println(object);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the line in which you're currently reading.
	 * 
	 * @return
	 */
	public String readLine() {
		String line = null;

		try {
			if (this.bufferedReader != null)
				line = this.bufferedReader.readLine();
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return line;
	}
}
