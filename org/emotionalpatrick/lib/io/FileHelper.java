package org.emotionalpatrick.lib.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.emotionalpatrick.lib.annotation.Experimental;

/**
 * Static utility methods that help with files.
 * 
 * @author Ruben Church
 */
@Experimental
public final class FileHelper {

	/**
	 * Reads then parses a configuration file into a {@link Configuration}
	 * object.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static Configuration readConfiguration(final File file)
			throws IOException {
		final Map<String, String> entries = new HashMap<String, String>();
		for (final String line : FileHelper.readLines(file))
			if (!line.isEmpty() && !line.startsWith(";")
					&& !line.startsWith("#")) {
				final String[] split = line.split("=");
				final StringBuilder builder = new StringBuilder();
				for (int i = 1; i < split.length; i++)
					builder.append(split[i]);
				entries.put(split[0], builder.toString());
			}
		return new Configuration(entries);
	}

	/**
	 * Reads then parses a configuration file into a {@link Configuration}
	 * object.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static Configuration readConfiguration(final String file)
			throws IOException {
		return FileHelper.readConfiguration(FileHelper.fileFrom(file));
	}

	/**
	 * Reads the lines of a file and returns them in a {@link List}.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static List<String> readLines(final File file) throws IOException {
		final List<String> lines = new ArrayList<String>();
		final BufferedReader reader = FileHelper.openReader(file);
		String line;
		while ((line = reader.readLine()) != null)
			lines.add(line);
		reader.close();
		return lines;
	}

	/**
	 * Reads the lines of a file and returns them in a {@link List}.
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static List<String> readLines(final String file) throws IOException {
		return FileHelper.readLines(FileHelper.fileFrom(file));
	}

	/**
	 * Opens a {@link BufferedReader} on a file.
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static BufferedReader openReader(final File file)
			throws FileNotFoundException {
		return new BufferedReader(new InputStreamReader(
				FileHelper.openStream(file)));
	}

	/**
	 * Opens a {@link BufferedReader} on a file.
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static BufferedReader openReader(final String file)
			throws FileNotFoundException {
		return FileHelper.openReader(FileHelper.fileFrom(file));
	}

	/**
	 * Opens an {@link InputStream} on a file.
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static InputStream openStream(final File file)
			throws FileNotFoundException {
		return new FileInputStream(file);
	}

	/**
	 * Opens an {@link InputStream} on a file.
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static InputStream openStream(final String file)
			throws FileNotFoundException {
		return FileHelper.openStream(FileHelper.fileFrom(file));
	}

	/**
	 * Returns a new <tt>File</tt> instance by converting the given pathname
	 * string into an abstract pathname. If the given string is the empty
	 * string, then the result is the empty abstract pathname.
	 * 
	 * @param file
	 * @return
	 */
	public static File fileFrom(final String file) {
		return new File(file);
	}

	/**
	 * <tt>FileHelper</tt> is a static-utility class and should therefore never
	 * be constructed.
	 * 
	 * @throws UnsupportedOperationException
	 *             If construction occurs.
	 */
	private FileHelper() {
		super();
	}

}