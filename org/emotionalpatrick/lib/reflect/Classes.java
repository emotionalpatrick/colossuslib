package org.emotionalpatrick.lib.reflect;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.emotionalpatrick.lib.annotation.Experimental;

/**
 * Static utility methods that allow you to reflect on the classes of the build
 * path.
 * 
 * @author Beyond Reality
 */
@Experimental
public final class Classes {

	/**
	 * Searches through all of the classes of the build path and returns a list
	 * containing them.
	 * 
	 * @return A list of all the classes of the build path.
	 */
	public static List<Class<?>> list() {
		final List<Class<?>> all = Classes.listFS();
		all.addAll(Classes.listAllWithinPackage());
		return all;
	}

	private static List<Class<?>> listAllWithinPackage() {
		return Classes.listWithinPackage("");
	}

	private static List<Class<?>> listWithinPackage(String packageName) {
		final List<Class<?>> classes = new ArrayList<Class<?>>();

		final CodeSource codeSource = Classes.class.getProtectionDomain()
				.getCodeSource();
		if (codeSource != null) {
			final String jarName = codeSource.getLocation().getFile();
			packageName = packageName.replaceAll("\\.", "/");

			final File jar = new File(jarName);
			if (jar.exists())
				try {
					final JarInputStream jarFile = new JarInputStream(
							new FileInputStream(jarName));

					JarEntry jarEntry;
					while ((jarEntry = jarFile.getNextJarEntry()) != null)
						if (jarEntry.getName().startsWith(packageName)
								&& jarEntry.getName().endsWith(".class"))
							classes.add(Class.forName(jarEntry
									.getName()
									.replaceAll("/", "\\.")
									.substring(0,
											jarEntry.getName().length() - 6)));

					jarFile.close();
				} catch (final IOException e) {
					e.printStackTrace();
				} catch (final ClassNotFoundException e) {
					e.printStackTrace();
				}
		}

		return classes;
	}

	private static List<Class<?>> listFS() {
		return Classes.listFSInside("");
	}

	private static List<Class<?>> listFSInside(final String packageName) {
		final List<Class<?>> classes = new ArrayList<Class<?>>();
		final String path = packageName.replace('.', '/');
		Enumeration<URL> roots;
		try {
			roots = Thread.currentThread().getContextClassLoader()
					.getResources(path);
		} catch (final IOException e) {
			return classes;
		}
		while (roots.hasMoreElements()) {
			final File root = new File(roots.nextElement().getPath());
			if (root != null && root.exists())
				classes.addAll(Classes.listInside(root, packageName));
		}

		return classes;
	}

	private static List<Class<?>> listInside(final File directory,
			final String packageName) {
		final List<Class<?>> classes = new ArrayList<Class<?>>();
		for (final File file : directory.listFiles())
			if (file.isDirectory())
				classes.addAll(Classes.listInside(file, packageName
						+ (!packageName.isEmpty() ? "." : "") + file.getName()));
			else if (file.getName().endsWith(".class"))
				try {
					classes.add(Class.forName(packageName
							+ '.'
							+ file.getName().substring(0,
									file.getName().length() - 6)));
				} catch (final ClassNotFoundException e) {
					continue;
				} catch (final NoClassDefFoundError e) {
					continue;
				}

		return classes;
	}

	/**
	 * <tt>Classes</tt> is a static-utility class and should therefore never be
	 * constructed.
	 * 
	 * @throws UnsupportedOperationException
	 *             If construction occurs.
	 */
	private Classes() {
		super();
	}

}