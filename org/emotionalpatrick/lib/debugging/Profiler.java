package org.emotionalpatrick.lib.debugging;

import org.emotionalpatrick.lib.annotation.Experimental;

@Experimental
public final class Profiler {
	private final long time;

	private Profiler(final long time) {
		this.time = time;
	}

	public long elapsed() {
		return System.currentTimeMillis() - this.time;
	}

	public static Profiler start() {
		return new Profiler(System.currentTimeMillis());
	}

}