package org.emotionalpatrick.lib.debugging;

import java.util.Random;

import org.emotionalpatrick.lib.annotation.Experimental;

@Experimental
public final class TimeManager {

	/**
	 * Used to convert milliseconds to an amount per second.
	 * 
	 * @param amountPerSecond
	 * @return
	 */
	public long convertToMilliseconds(final double amountPerSecond) {
		return (long) (1000 / amountPerSecond);
	}

	/**
	 * Used to return a random number between a minimum value and a maximum
	 * value.
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public int getRandomNumberBetween(final int min, final int max) {
		final Random random = new Random();
		final int randomNumber = random.nextInt(max - min) + min;
		if (randomNumber == min)
			return min + 1;
		else
			return randomNumber;
	}

	/**
	 * Used to get the current milliseconds.
	 * 
	 * @return
	 */
	public long getMilliseconds() {
		return System.nanoTime() / 1000000L;
	}

}
