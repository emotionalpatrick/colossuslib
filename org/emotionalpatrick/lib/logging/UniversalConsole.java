package org.emotionalpatrick.lib.logging;

import java.util.Scanner;

import org.emotionalpatrick.lib.annotation.Experimental;

/**
 * Used to access the character-based console device by making use of both
 * {@link java.io.Console} and {@link Scanner}; with preference to
 * <tt>java.io.Console</tt>.
 * 
 * @author Ruben Church
 */
@Experimental
public final class UniversalConsole implements Console {

	/**
	 * The backing <i>Java</i> console that is used with preference over
	 * {@link Scanner}.
	 */
	private final java.io.Console console;

	/**
	 * The backing <i>Java</i> scanner. Note that {@link #console} will take
	 * preference when possible.
	 */
	private final Scanner scanner;

	/**
	 * Constructs a new universal console.
	 * 
	 * @param console
	 *            The backing <i>Java</i> console that is used with preference
	 *            over {@link Scanner}.
	 * @param scanner
	 *            The backing <i>Java</i> scanner. Note that {@link #console}
	 *            will take preference when possible.
	 */
	public UniversalConsole(final java.io.Console console, final Scanner scanner) {
		this.console = console;
		this.scanner = scanner;
	}

	/**
	 * Constructs a new universal console; supplying {@link System#console} for
	 * the backing console and creates a new {@link Scanner} on
	 * {@link System#in}.
	 */
	public UniversalConsole() {
		this(System.console(), new Scanner(System.in));
	}

	@Override
	public void print(final String text) {
		System.out.print(text);
	}

	@Override
	public void println(final String text) {
		System.out.println(text);
	}

	@Override
	public void println() {
		println("");
	}

	@Override
	public void printf(final String format, final Object... args) {
		System.out.printf(format, args);
	}

	@Override
	public String readLine() {
		if (this.console == null)
			return this.scanner.nextLine();
		return this.console.readLine();
	}

	@Override
	public String readLine(final String prompt) {
		print(prompt + " ");
		return readLine();
	}

	@Override
	public String readPassword() {
		if (this.console == null)
			return readLine();
		return String.valueOf(this.console.readPassword());
	}

	@Override
	public String readPassword(final String prompt) {
		print(prompt + " ");
		return readPassword();
	}

}