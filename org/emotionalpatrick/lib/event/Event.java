package org.emotionalpatrick.lib.event;

/**
 * A thing that happens; an occurrence.
 * 
 * <p>
 * An event is characterized by having a type and relative information about
 * each occurrence of the event.
 * </p>
 * 
 * @author Ruben Church
 */
public class Event {
	private boolean cancelled = false;

	public String getName() {
		return getClass().getSimpleName();
	}

	public boolean isCancelled() {
		return this.cancelled;
	}

	public void setCancelled(final boolean cancelled) {
		this.cancelled = cancelled;
	}

}
