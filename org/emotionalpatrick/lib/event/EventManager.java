package org.emotionalpatrick.lib.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * An event manager provides support for dynamic {@link Event} dispatching along
 * with {@link EventListener} registration.
 * 
 * @author Ruben Church
 */
public final class EventManager {
	private final List<EventSender> eventSenders;

	public EventManager() {
		this.eventSenders = new ArrayList<EventSender>();
	}

	public synchronized void registerListener(final EventListener listener) {
		final Class<? extends EventListener> listenerClass = listener
				.getClass();
		for (final Method method : listenerClass.getMethods()) {
			if (method.getAnnotation(EventHandler.class) == null)
				continue;
			if (!method.isAccessible())
				method.setAccessible(true);
			if (method.getParameterTypes().length != 1)
				throw new IllegalArgumentException("Method "
						+ method.toString() + " in class "
						+ listenerClass.getName()
						+ " has incorrect amount of parameters");
			final Class<? extends Event> eventClass = method
					.getParameterTypes()[0].asSubclass(Event.class);
			boolean senderExists = false;
			for (final EventSender sender : this.eventSenders)
				if (eventClass.isAssignableFrom(sender.getListenerEventClass())) {
					sender.addHandler(listener, method);
					senderExists = true;
				}
			if (!senderExists) {
				final EventSender sender = new EventSender(eventClass);
				this.eventSenders.add(sender);
				sender.addHandler(listener, method);
			}
		}
	}

	public synchronized void unregisterListener(final EventListener listener) {
		for (final EventSender sender : this.eventSenders)
			sender.unregisterListener(listener);
	}

	public synchronized void clearListeners() {
		this.eventSenders.clear();
	}

	public synchronized void sendEvent(final Event event) {
		final List<EventSender> sendTo = new ArrayList<EventSender>();
		for (final EventSender sender : this.eventSenders) {
			final Class<? extends Event> eventClass = sender
					.getListenerEventClass();
			if (eventClass.isInstance(event))
				sendTo.add(sender);
		}
		for (final EventSender sender : sendTo)
			sender.sendEvent(event);
	}

	public synchronized List<EventListener> getListeners(
			final Class<? extends Event> eventClass) {
		for (final EventSender sender : this.eventSenders)
			if (eventClass.isAssignableFrom(sender.getListenerEventClass()))
				return sender.getListeners();
		return new ArrayList<EventListener>();
	}
}
