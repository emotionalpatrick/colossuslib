package org.emotionalpatrick.lib.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An event sender can add and remove listeners and send events. Successfully
 * implemented by @link EventManager.
 * 
 * @author Ruben Church
 */
final class EventSender {
	private final Map<EventListener, List<Method>> handlers;
	private final Class<? extends Event> listenerEventClass;

	public EventSender(final Class<? extends Event> listenerEventClass) {
		this.handlers = new HashMap<EventListener, List<Method>>();
		this.listenerEventClass = listenerEventClass;
	}

	public synchronized void addHandler(final EventListener listener,
			final Method method) {
		List<Method> methods = this.handlers.get(listener);
		if (methods == null) {
			methods = new ArrayList<Method>();
			this.handlers.put(listener, methods);
		}
		methods.add(method);
	}

	public synchronized void unregisterListener(final EventListener listener) {
		this.handlers.remove(listener);
	}

	public synchronized List<EventListener> getListeners() {
		return new ArrayList<EventListener>(this.handlers.keySet());
	}

	public synchronized void sendEvent(final Event event) {
		final Class<?> eventClass = event.getClass();
		if (!eventClass.isAssignableFrom(this.listenerEventClass))
			return;
		for (final EventListener listener : new ArrayList<EventListener>(
				this.handlers.keySet())) {
			final List<Method> methods = this.handlers.get(listener);
			if (methods == null)
				continue;
			for (final Method method : methods)
				try {
					method.invoke(listener, event);
				} catch (final Throwable exception) {
					exception.printStackTrace();
				}
		}
	}

	public synchronized Class<? extends Event> getListenerEventClass() {
		return this.listenerEventClass;
	}
}
