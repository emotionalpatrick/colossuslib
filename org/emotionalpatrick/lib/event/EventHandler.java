package org.emotionalpatrick.lib.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to mark a method as a delegate for the handling of an event dispatch.
 * 
 * <p>
 * Event handler methods should contain the {@link Event} type that they are
 * expected to handle as a single argument. That is, they should only have one
 * argument, which is the {@link Event} type that they should handle.
 * </p>
 * 
 * @author Ruben Church
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {
}